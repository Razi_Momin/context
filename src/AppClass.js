import React, { Component } from 'react'
import Header from './Header'
import Footer from './Footer'
import Home from './Home'
import { CommonContext } from './CommonContext';
export default class App extends Component {
  constructor(props) {
    // alert(CryptoJS.MD5("Message"))
    super();
    this.updateColor = (color) => {
      this.setState({
        color: color
      })
    }
    this.state = {
      mobile_no: '',
      color: ' ',
      updateColor: this.updateColor
    }
    // console.log(gVars.API_BASE_URL);
  }
  render() {
    return (
      <div>
        <CommonContext.Provider value={this.state}>
          <Home />
          <Header />
          <Footer />
        </CommonContext.Provider>
      </div>
    )
  }
}
