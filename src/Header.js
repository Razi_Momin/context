import React, { Component } from 'react'

import { CommonContext } from './CommonContext';
export default class Header extends Component {
    constructor(props) {
        // alert(CryptoJS.MD5("Message"))
        super();

        // this.state = {
        //     mobile_no: '',
        //     // color: null,
        //     updateColor: this.updateColor
        // }

        // console.log(gVars.API_BASE_URL);
    }
    updateColor1 = () => {
        this.setState({
            color: 'red'
        })
    }
    // updateColor1() {
    //     this.setState({
    //         date: new Date()
    //     });
    // }
    render() {
        console.log(this.state);
        return (
            <div>
                <CommonContext.Consumer>
                    {
                        ({ color }) => (

                            <h1 style={{ backgroundColor: color }}>Hello this is header page</h1>
                        )
                    }
                </CommonContext.Consumer>
                <button onClick={this.updateColor1}>Update color</button>
            </div>
        )
    }
}
