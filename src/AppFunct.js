import React from 'react'
const MyContext = React.createContext();

const Coder3 = () => {
    return (
        <MyContext.Consumer>
            {
                (data) => {
                    return (
                        <React.Fragment>
                            <h1>My name is {data.name}</h1>
                            <h1>My age is {data.age}</h1>
                        </React.Fragment>
                    )
                }
            }
        </MyContext.Consumer>
    )
}
const Coder2 = () => {
    return <Coder3 />
}
const Coder = () => {
    return <Coder2 />
}
export default function App() {
    return (
        <div>
            <MyContext.Provider value={{ name: 'razi', age: 27 }}>
                <Coder />
            </MyContext.Provider>
        </div>
    )
}
