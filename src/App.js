import React, { useContext, useReducer, createContext } from 'react';
// import Coder from './components/Coder';
// import Coder1 from './components/Component1';

// import Component1 from './components/Component1';
const Mycontext = createContext()
const iState = {
    name: 'razi--',
    age: 25
}
const reducer = (state, action) => {
    switch (action.type) {
        case 'CHANGE_NAME':
            return {
                ...state,
                name: action.payload
            }
        case 'CHANGE_AGE':
            return {
                ...state,
                age: action.payload
            }
        default:
            return {
                state
            }
    }
}
const Coder3 = () => {
    const myname = useContext(Mycontext)
    return (
        <div>
            <h1>
                Coder 3
            </h1>
            <h2>{myname.name}</h2>
            <button onClick={() => myname.changeName({ type: 'CHANGE_NAME', payload: "akshay" })}>Lets change it</button>
        </div>
    )
}
const Coder2 = () => {
    const myname = useContext(Mycontext)
    return (
        <div>
            <h1>
                Coder 2
            </h1>
            <h2>{myname.name}</h2>
            <button onClick={() => myname.changeName({ type: 'CHANGE_NAME', payload: "momin" })}>Lets change it</button>
            <Coder3 />
        </div>
    )
}
const Coder = () => {
    const myname = useContext(Mycontext)
    return (
        <div>
            <h1>
                Coder 1
            </h1>
            <h2>{myname.age}</h2>
            <button onClick={() => myname.changeName({ type: 'CHANGE_AGE', payload: "27" })}>Change Age</button>
            <Coder2 />
        </div>
    )
}
export default function App() {
    const [data, dispatch] = useReducer(reducer, iState)
    return (
        <Mycontext.Provider value={{ name: data.name, age: data.age, changeName: dispatch }}>
            <div className="App">
                <Coder />
            </div>
        </Mycontext.Provider>

    )
}
