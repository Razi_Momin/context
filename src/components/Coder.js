// import React, { Component } from 'react'
 const CommonContext = React.createContext();
export default class Coder extends Component {
    render() {
        return (
            <div>
                <CommonContext.Consumer>
                    {
                        ({ name }) => (
                            { name }
                        )
                    }
                </CommonContext.Consumer>

            </div>
        )
    }
}
